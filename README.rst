Kwant Gallery
=============

`Kwant <http://kwant-project.org>`_ makes defining, solving, and exploring
tight-binding models a breeze. While Kwant is a powerful library with
reasonably complete documentation and tutorial, it lacks a comprehensive
gallery of examples and reusable models that can be used as a starting
point for exploration.
