# Copyright 2017
# Licensed under the New BSD License. See the LICENCE file in the project root
# for full license information.
"""A collection of reusable Kwant models.

This module contains functions that return `kwant.Builder`s that define various
physical models. Typically the returned `~kwant.Builder`s will have one or more
translational symmetries and will depend on value functions that depend on
parameters. The expected parameters are explained in the ``System Parameters``
sections of the individual function documentation.

The main use for these models is to fill existing `~kwant.Builder`s
(which possibly have a lower symmetry) by using the `~kwant.Builder.fill`
method.
"""

import tinyarray as ta
import kwant
from kwant import lattice, Builder
from kwant.builder import HoppingKind


__all__ = ['kane_mele']


sigma_0 = ta.array([[1, 0], [0, 1]])
sigma_x = ta.array([[0, 1], [1, 0]])
sigma_y = ta.array([[0, -1j], [1j, 0]])
sigma_z = ta.array([[1, 0], [0, -1]])


## NOTE: make all value functions available at global scope so that the
##       resulting systems may be pickled.

def _km_nn_hopping(i, j, t):
    return t * sigma_0


def _km_nnn_hopping(i, j, t2):
    return 1j * t2 * sigma_z


def kane_mele():
    """The Kane-Mele model for quantum spin Hall effect on a honeycomb lattice.

    Returns a `kwant.Builder` with a 2D translational symmetry that defines
    the Kane-Mele model as described in equation (6) of [1]_.

    System Parameters
    -----------------
    t : real
        Nearest-neighbor hopping parameter
    t1 : real
        Next-nearest-neighbor hopping parameter

    .. [1] `Phys. Rev. Lett. 95, 226801 (2005)
           <https://doi.org/010.1103/PhysRevLett.95.226801>`_
    """

    lat = lattice.honeycomb(norbs=2)

    T = kwant.TranslationalSymmetry(lat.vec((1, 0)), lat.vec((0, 1)))
    syst = kwant.Builder(T)

    a, b = lat.sublattices
    # multiply by sigma_0 to get the correct 2x2 shape
    syst[a(0, 0)] = 0 * sigma_0
    syst[b(0, 0)] = 0 * sigma_0

    # these define the positive sign direction for each sublattice
    nnn_hoppings_a = [((1, 0), a, a), ((0, -1), a, a), ((-1, 1), a, a)]
    nnn_hoppings_b = [((-1, 0), b, b), ((0, 1), b, b), ((1, -1), b, b)]
    nnn_hoppings = nnn_hoppings_a + nnn_hoppings_b

    syst[lat.neighbors(1)] = _km_nn_hopping

    syst[(HoppingKind(*kind) for kind in nnn_hoppings)] = _km_nnn_hopping

    return syst
